<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Altynai;
use Redirect;
use Input;
class AltynaiController extends Controller{

	public function index(){
		//echo "this is my index page hello";
		//return view('Altynai_index');
		
		$Altynai = Altynai::paginate(5);
		return view('Altynai_index', compact('Altynai') );	
	}
	public function create(){
		return view('Altynai_create');
	}
	// insert to DB here
	public function store(Request $request){	
		$Altynai            = new Altynai();
		$Altynai  -> name    = $request -> Altynai_name;
		$Altynai  -> address = $request -> Altynai_address;
		$Altynai  -> save();
		return redirect() -> route('Altynai.index');	
	}
	public function edit($id)
	{
		$Altynai = Altynai::find($id);
		return view('Altynai_edit', compact('Altynai'));
	}
	public function update(Request $request, $id)
	{
		$Altynai = Altynai::find($id);
		$Altynai -> name    = $request -> Altynai_name;
		$Altynai -> address = $request -> Altynai_address;
		$Altynai -> save();
		return redirect() -> route('Altynai.index');	
	}
	public function destroy($id)
	{
		$Altynai = Altynai::find($id);
		$Altynai -> delete();
		return redirect() -> route('Altynai.index');
	}
}
?>
